// ---- START VEXCODE CONFIGURED DEVICES ----
// Robot Configuration:
// [Name]               [Type]        [Port(s)]
// ctlr                 controller
// leftFront            motor         1
// leftBack             motor         2
// rightBack            motor         9
// rightFront           motor         10
// ClawMotor            motor         3
// ArmMotor             motor         8
// ---- END VEXCODE CONFIGURED DEVICES ----

#define ftcisbetterthanvex vex
#define ftcisbetterthanvexcodeInit() vexcodeInit()
#define right_stick_x Axis1.position()
#define right_stick_y Axis2.position()
#define left_stick_y Axis3.position()
#define left_stick_x Axis4.position()
#define elif else if
#define luaisbestlanguage true

#include "vex.h"

using namespace ftcisbetterthanvex;

double turnspeedfactor = 1;//1/40;
double mainspeedfactor = 1;//1/40;

//Move dem fuckin wheels
void mecanumdrive( double turn, double angle, double magnitude ) {
  double lFrB; double rFlB;
  if (magnitude > 1) magnitude = 1; //jic of user error
  

  if (magnitude != 0) { //If turning is not the only movement we're doing
    lFrB = sin(angle + (M_PI/4));
    rFlB = sin(angle - (M_PI/4));
  } else { //If we're only turning, we don't need the sin fxns so we just set to 0 and add/sub turn normally
    lFrB = 0;
    rFlB = 0;
  }

  //Apply turn modifier
  double lF = lFrB+turn;
  double rF = rFlB-turn;
  double lB = rFlB+turn;
  double rB = lFrB-turn;

  if (magnitude != 0) { //If turning is not the only movement we're doing

    //Sets a standard ratio to be modified later so we don't have values over 1 (or under -1) without distorting the velocity ratios between each motor.
    double maxMotorPwr = fmax( fmax( std::abs(lF), std::abs(rF) ) , fmax( std::abs(lB), std::abs(rB) ) );

    lF /= maxMotorPwr;
    rF /= maxMotorPwr;
    lB /= maxMotorPwr;
    rB /= maxMotorPwr;

  }

  if (magnitude == 0) magnitude = 1; //So I don't need another big if statement if we're only turning

  //applying speed modifiers (magnitude is distance of joystick from zero point)
  //mainspeedfactor = 1 but is there so it can be mod. by ctlr l8r.
  //Since the motor powers are in the range [-1,1], mult by 100 so setVelocity sees them as [-100%,100%]
  lF *= magnitude * mainspeedfactor * 100;
  rF *= magnitude * mainspeedfactor * 100;
  lB *= magnitude * mainspeedfactor * 100;
  rB *= magnitude * mainspeedfactor * 100;

  //Apply the Velocities
  leftFront.setVelocity(lF, percent); leftFront.spin(forward);
  rightFront.setVelocity(rF, percent); rightFront.spin(forward);
  leftBack.setVelocity(lB, percent); leftBack.spin(forward);
  rightBack.setVelocity(rB, percent); rightBack.spin(forward);

}

//Stop dem fuckin wheels
void stopmotors() {
  leftFront.stop();
  rightFront.stop();
  leftBack.stop();
  rightBack.stop();
}

int main() {
  // Initializing Robot Configuration. DO NOT REMOVE!
  ftcisbetterthanvexcodeInit();

  while (luaisbestlanguage) { //OpMode Loop

    //Mecanum Drive
    if(ctlr.left_stick_x != 0 || ctlr.left_stick_y != 0 || ctlr.right_stick_x != 0) {
      double angle = atan2(-1 * ctlr.left_stick_y, -1 * ctlr.left_stick_x); //Find what radian the joystick is pointing to (think unit circle w/ pi/2 as fwd)
      double turn = -1 * ctlr.right_stick_x; //Controls turning
      double magnitude = sqrt( pow(ctlr.left_stick_y,2) + pow(ctlr.left_stick_x,2) ); //Dist formula to find dist from the stick to the origin
      magnitude /= 100;
      turn /= 100;
      if(magnitude!=0){
      Brain.Screen.print("=%f", magnitude);
      wait(0.1,seconds);
      Brain.Screen.clearLine();
      }mecanumdrive(turn, angle, magnitude*0.75); //make it do the truffle shuffle

    } else stopmotors(); //stop the truffle shuffle in its tracks

    //Claw
    if (ctlr.ButtonL1.pressing()) {
      //Close Claw
      ClawMotor.spin(forward, 100, percent);
    }
    elif (ctlr.ButtonL2.pressing()) {
      //Open Claw
      ClawMotor.spin(reverse, 100, percent);
    } else ClawMotor.stop();

    //Arm
    if (ctlr.ButtonR1.pressing()) {
      //Arm comes up
      ArmMotor.spin(forward, 75, percent);
    }
    elif (ctlr.ButtonR2.pressing()) {
      //Arm goes down
      ArmMotor.spin(reverse, 75, percent);
    } else ArmMotor.stop();
  }
}
