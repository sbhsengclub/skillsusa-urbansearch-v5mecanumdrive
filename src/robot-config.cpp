#include "vex.h"

#define ftcisbetterthanvex vex

using namespace ftcisbetterthanvex;
using signature = vision::signature;
using code = vision::code;

// A global instance of brain used for printing to the V5 brain screen
brain Brain;

// VEXcode device constructors
controller ctlr = controller(primary);

motor leftFront = motor(PORT1, ratio18_1, true);       motor rightFront = motor(PORT10, ratio18_1, false);
motor leftBack = motor(PORT2, ratio18_1, true);        motor rightBack = motor(PORT9, ratio18_1, false);

motor ClawMotor = motor(PORT3, ratio18_1, false);

motor ArmMotor = motor(PORT8, ratio18_1, false);

//Inits robot
void vexcodeInit(void) {

  // BrakeType::hold actively uses pid to make sure the motor stays in the set position
  // BrakeType::brake shorts the motor so it refuses to budge, as motors do

  // Set ZeroPowerBehavior, but Vex version
  leftFront.setBrake(brake);
  rightFront.setBrake(brake);
  leftBack.setBrake(brake);
  rightBack.setBrake(brake);
  // idk mate maybe they do the same thing, but why not both jic?
  leftFront.setStopping(brake);
  rightFront.setStopping(brake);
  leftBack.setStopping(brake);
  rightBack.setStopping(brake);

  ClawMotor.setBrake(hold);
  ClawMotor.setStopping(hold);

  ArmMotor.setBrake(hold);
  ArmMotor.setStopping(hold);
}